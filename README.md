R scripts to run WORMSIM (versions 4.7.0, which requires Java 17), including convenience functions to manipulate input and collect model output.

To use these scripts, make sure to (1) copy the appropriate version of WORMSIM4 to the folder "03_WORMSIM4" and (2) update the "base_dir" path in "01_Code/use_example.r".

Note that although these R scripts are updated to work with WORMSIM v4.7.0, not all features of this WORMSIM version are necessarily accessible via the R functions, such as the possibility to now store population states and re-use them as starting points for new simulations. This still requires further development of the R scripts.
