# Functions to substitute parameter values in a parsed XML files. All functions
# follow the naming convention "set_[name]", where [name] refers to the
# parameter for which the value(s) is/are to be substituted. All functions take
# two arguments:
#   xml_doc = object holding a parsed XML file, created with read_xml_file().
#   pars = named list of parameter value(s).

# Function to get the right XML environment ----
get_xml_env <- function(xml_doc,
                        xml_path,
                        ns = xmlNamespaceDefinitions(xml_doc,
                                                     simplify = TRUE)[[1]]) {
  # Return the environment from the given namespace
  xpathApply(doc = xml_doc,
             path = gsub(pattern = "/",
                         replacement = "/r:",
                         x = xml_path),
             namespaces = c("r" = ns))
}


# Function to check correctness of parameter names ----
check_par_names <- function(x,
                            required,
                            fun_name = "unknown_function") {
  
  if (!all(names(x) == required))
    stop(paste0("Par inputs for ", fun_name, " must be: ",
                paste0(required, collapse = " ")))
  
}


# Function to check of length of parameters is correct ----
check_par_length <- function(x,
                             required = 1,
                             fun_name = "unknown_function") {
  
  if (length(x) != required)
    stop(paste0("Length of par input for ", fun_name,
                " must be of length ", required))
  
}

# Set simulation time period ----
set_simulation_time <- function(xml_doc = NULL, par = NULL) {
  
  check_par_names(x = par,
                  required = c("start", "stop", "update_interval"))
  
  xpath_expr <-  "/wormsim.input/simulation"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["start"] <- paste(par$start)
  xmlAttrs(env[[1]])["stop"] <- paste(par$stop)
  xmlAttrs(env[[1]])["progressIndicatorInterval"] <- paste(par$update_interval)
  return(xml_doc)
  
}


# Set maximum human population size ----
set_population_max <- function(xml_doc = NULL, par = NULL) {
  
  check_par_names(x = par,
                  required = c("max_size", "fraction_removed"))
  
  xpath_expr <-  "/wormsim.input/demography/population.max"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["max.size"] <- paste(par$max_size)
  xmlAttrs(env[[1]])["fraction.killed"] <- paste(par$fraction_removed)
  return(xml_doc)
  
}

# Set adult worm lifespan ----
set_adult_lifespan <- function(xml_doc = NULL,
                               par = NULL) {
  
  check_par_names(x = par,
                  required = c("dist", "mean", "param"),
                  fun_name = "set_adult_lifespan")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/lifespan"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["dist"] <- paste(par$dist)
  xmlAttrs(env[[1]])["mean"] <- paste(par$mean)
  xmlAttrs(env[[1]])["param"] <- paste(par$param)
  return(xml_doc)
  
}


# Set pre-patent period ----
set_prepatent_period <- function(xml_doc = NULL,
                                 par = NULL) {
  
  check_par_length(x = par,
                  required = 1,
                  fun_name = "set_prepatent_period")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/prepatent.period"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["value"] <- paste(par)
  return(xml_doc)
  
}


# Set mating ----
set_mating <- function(xml_doc = NULL,
                       par = NULL) {
  
  check_par_names(x = par,
                  required = c("insemination_exp", "male_potention"),
                  fun_name = "set_mating")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/mating/polygamous"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["insemination.expiry.in.cycles"] <- paste(par$insemination_exp)
  xmlAttrs(env[[1]])["male.potention"] <- paste(par$male_potention)
  return(xml_doc)
  
}


# Set fertility ----
set_fertility <- function(xml_doc = NULL,
                          par = NULL) {
  
  check_par_names(x = par,
                  required = c("fecundity_lambda",
                               "offspring_per_Fworm",
                               "dens_dep"),
                  fun_name = "set_fertility")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["fecundity.lambda"] <- paste(par$fecundity_lambda)
  xmlAttrs(env[[1]])["offspring.per.Fworm"] <- paste(par$offspring_per_Fworm)
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility/density.dependence"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["func"] <- paste(par$dens_dep$func)
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility/density.dependence/param"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["value"] <- paste(par$dens_dep$a)
  xmlAttrs(env[[2]])["dist"] <- paste(par$dens_dep$b$dist)
  xmlAttrs(env[[2]])["mean"] <- paste(par$dens_dep$b$mean)
  xmlAttrs(env[[2]])["param"] <- paste(par$dens_dep$b$param)
  xmlAttrs(env[[3]])["value"] <- paste(par$dens_dep$c)
  
  return(xml_doc)
  
}


# Set fertility density dependence ----
set_fertility_dd <- function(xml_doc = NULL,
                             par = NULL) {
  
  check_par_names(x = par,
                  required = c("func",
                               "a",
                               "b",
                               "c"),
                  fun_name = "set_fertility_dd")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility/density.dependence"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["func"] <- paste(par$func)
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility/density.dependence/param"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["value"] <- paste(par$a)
  xmlAttrs(env[[2]])["dist"] <- paste(par$b$dist)
  xmlAttrs(env[[2]])["mean"] <- paste(par$b$mean)
  xmlAttrs(env[[2]])["param"] <- paste(par$b$param)
  xmlAttrs(env[[3]])["value"] <- paste(par$c)
  
  return(xml_doc)
  
}


# Set offspring production (by worm age, offspring sex) ----
set_offspring_prod <- function(xml_doc = NULL,
                               par = NULL) {
  
  check_par_names(x = par,
                  required = c("male_fract",
                               "age_bin",
                               "production"),
                  fun_name = "set_offspring_prod")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility/offspring.production.table"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["fraction.males"] <- paste(par$male_fract)
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/fertility/offspring.production.table/offspring.production"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par$age_bin)) {
    xmlAttrs(env[[i]])["upper.age.limit"] <- paste(par$age_bin[i])
    xmlAttrs(env[[i]])["production"] <- paste(par$production[i])
  }
  
  return(xml_doc)
  
}


# Set offspring lifespan ----
set_offspring_lifespan_max <- function(xml_doc = NULL,
                                       par = NULL) {
  
  check_par_length(x = par,
                  required = 1,
                  fun_name = "set_offspring_lifespan_max")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/parasite/offspring.lifespan/fixed"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["max.in.cycles"] <- paste(par)
  
  return(xml_doc)
  
}


# Set transmission rate ----
set_transm_rate <- function(xml_doc = NULL, par = NULL) {
  
  check_par_length(x = par,
                   required = 1,
                   fun_name = "set_transm_rate")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["contribution.multiplier"] <- paste(par)
  return(xml_doc)
  
}


# Set monthly transmission multiplier ----
set_transm_season_multiplier <- function(xml_doc = NULL,
                                         par = NULL) {
  
  check_par_length(x = par,
                  required = 12,
                  fun_name = "set_transm_season_multiplier")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission/seasonality.table/seasonality"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:12) {
    xmlAttrs(env[[i]])["rate"] <- paste(par[i])
  }
  
  return(xml_doc)
  
}


# Set exposure heterogeneity distribution ----
set_expo_het <- function(xml_doc = NULL, par = NULL) {
  
  check_par_names(x = par,
                  required = c("dist", "mean", "param", "max"))
  
  xpath_expr <-  "/wormsim.input/transmission.pattern.table/transmission.pattern/exposure.individual.index"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["dist"] <- paste(par$dist)
  xmlAttrs(env[[1]])["mean"] <- paste(par$mean)
  xmlAttrs(env[[1]])["param"] <- paste(par$param)
  xmlAttrs(env[[1]])["max"] <- paste(par$max)
  return(xml_doc)
  
}


# Set exposure heterogeneity parameter ----
set_expo_het_shape <- function(xml_doc = NULL, par = NULL) {
  
  xpath_expr <-  "/wormsim.input/transmission.pattern.table/transmission.pattern/exposure.individual.index"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["param"] <- paste(par)
  return(xml_doc)
  
}


# Set age patterns in exposure ----
set_exposure_age <- function(xml_doc = NULL,
                             par = NULL) {
  
  check_par_names(x = par,
                  required = c("func", "min", "max", "param"),
                  fun_name = "set_exposure_age")
  
  xpath_expr <-  "/wormsim.input/transmission.pattern.table/transmission.pattern/exposure.age.dependency/age.function"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["func"] <- paste(par$func)
  xmlAttrs(env[[1]])["min"] <- paste(par$min)
  xmlAttrs(env[[1]])["max"] <- paste(par$max)
  
  xpath_expr <-  "/wormsim.input/transmission.pattern.table/transmission.pattern/exposure.age.dependency/age.function/param"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par$param)) {
    xmlAttrs(env[[i]])["value"] <- paste(par$param[i])
  }
  
  return(xml_doc)
  
}


# Set age patterns in contribution ----
set_contrib_age <- function(xml_doc = NULL,
                            par = NULL) {
  
  check_par_names(x = par,
                  required = c("func", "min", "max", "param"),
                  fun_name = "set_contrib_age")
  
  xpath_expr <-  "/wormsim.input/transmission.pattern.table/transmission.pattern/contribution.age.dependency/age.function"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["func"] <- paste(par$func)
  xmlAttrs(env[[1]])["min"] <- paste(par$min)
  xmlAttrs(env[[1]])["max"] <- paste(par$max)
  
  xpath_expr <-  "/wormsim.input/transmission.pattern.table/transmission.pattern/contribution.age.dependency/age.function/param"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par$param)) {
    xmlAttrs(env[[i]])["value"] <- paste(par$param[i])
  }
  
  return(xml_doc)
  
}


# Set success probability in cloud ----
set_succes_prob_in_cloud <- function(xml_doc = NULL,
                                     par = NULL) {
  
  check_par_length(x = par,
                   required = 1,
                   fun_name = "set_succes_prob_in_cloud")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["success.probability.in.cloud"] <- paste(par)
  
  return(xml_doc)
  
}


# Set success probability in host ----
set_succes_prob_in_host <- function(xml_doc = NULL,
                                    par = NULL) {
  
  check_par_length(x = par,
                   required = 1,
                   fun_name = "set_succes_prob_in_host")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["success.probability.in.host"] <- paste(par)
  
  return(xml_doc)
  
}


# Set function for uptake in reservoir/vector ----
set_reservoir_uptake <- function(xml_doc = NULL,
                                 par = NULL) {
  
  check_par_names(x = par,
                  required = c("func", "param"),
                  fun_name = "set_reservoir_uptake")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission/uptake.function"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["func"] <- paste(par$func)
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission/uptake.function/param"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par$param)) {
    xmlAttrs(env[[i]])["value"] <- paste(par$param[i])
  }
  
  return(xml_doc)
  
}


# Set function for survival in reservoir ----
set_reservoir_surv <- function(xml_doc = NULL,
                               par = NULL) {
  
  check_par_names(x = par,
                  required = c("life_span", "multiplier"),
                  fun_name = "set_reservoir_surv")
  check_par_length(x = par$multiplier, required = 12)
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission/reservoir.survival.table"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["life.span"] <- paste0(par$life_span, "d")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission/reservoir.survival.table/survival"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par$multiplier)) {
    xmlAttrs(env[[i]])["multiplier"] <- paste(par$multiplier[i])
  }

  return(xml_doc)
  
}


# Set external force of infection ----
set_foi_ext <- function(xml_doc = NULL,
                        par = NULL) {
  
  check_par_names(x = par,
                  required = c("start", "value", "exclusive"),
                  fun_name = "set_foi_ext")
  if (with(par,
           length(start) != length(value) || length(start) != length(exclusive)))
    stop("the 'start', 'value', and 'exclusive' argumnents to 'set_foi_ext()' must be of equal length")
  
  xpath_expr <-  "/wormsim.input/disease.table/disease/transmission/external.foi.table/external.foi"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par$start)) {
    xmlAttrs(env[[i]])["start"] <- paste(par$start[i])
    xmlAttrs(env[[i]])["value"] <- paste(par$value[i])
    xmlAttrs(env[[i]])["exclusive"] <- paste(par$exclusive[i])
  }
  
  return(xml_doc)
  
}


# Set adulticidal effect of drug treatment ----
set_treatm_adult_worm_kill <- function(xml_doc = NULL, par = NULL) {
  
  check_par_length(x = par,
                   required = 1,
                   fun_name = "set_treatm_adult_worm_kill")
  
  xpath_expr <-  "/wormsim.input/control/mass.treatment/drug.table/drug/effect.table/effect/adult/fraction.killed"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["value"] <- paste(par)
  return(xml_doc)
  
}


# Set up a sequence of PC rounds ----
set_pc_seq <- function(xml_doc = NULL, par = NULL) {
  
  check_par_names(x = par,
                  required = c("start", "periodicity", "stop",
                               "coverage", "fraction_excluded",
                               "systematicness"),
                  fun_name = "set_pc_seq")
  
  xpath_expr <-  "/wormsim.input/control/mass.treatment/round.table/round"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["start"] <- paste(par$start)
  xmlAttrs(env[[1]])["periodicity"] <- paste(par$periodicity)
  xmlAttrs(env[[1]])["stop"] <- paste(par$stop)
  xmlAttrs(env[[1]])["coverage"] <- paste(par$coverage)
  xmlAttrs(env[[1]])["fraction.excluded"] <- paste(par$fraction_excluded)
  xmlAttrs(env[[1]])["systematicness"] <- paste(par$systematicness)
  return(xml_doc)
  
}


# Set participation weights (age/sex) for PC ----
set_pc_part_weights <- function(xml_doc = NULL,
                                par = NULL) {
  
  check_par_names(x = par,
                  required = c("ages", "weights"),
                  fun_name = "set_pc_part_weights")
  if (length(par$ages) != nrow(par$weights))
    stop("'set_pc_part_weights()' requires that the length of argument 'ages' equals the number of rows of argument 'weights'")
  
  # Check how many placeholders there are
  xpath_expr <-  "/wormsim.input/core.participation.tables/participation.table"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  children <- xmlChildren(xml_doc)$wormsim.input[["core.participation.tables"]][["participation.table"]]
  max_rows <- length(xmlChildren(children))
  
  # Trim input (and throw warning) if the number of placeholders cannot
  # accommodate all inputs
  if (length(par$ages) > max_rows) {
    par$ages <- par$ages[1:max_rows]
    par$weights <- par$weights[1:max_rows, ]
    warning(paste0("'set_pc_part_weights()' will only use the first ",
                   max_rows, " input values provides as the xml template does not provide a sufficient number of placeholders"))
  }
  
  # Trim placeholders that will not be filled with inputs (if any)
  if (length(par$ages) < max_rows) {
    removeChildren(children,
                   kids = as.list((length(par$ages) + 1):max_rows))
  }
  
  # Insert inputs into placeholders
  xpath_expr <-  "/wormsim.input/core.participation.tables/participation.table/participation"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  
  for (i in 1:length(par$ages)) {
    xmlAttrs(env[[i]])["upper.age.limit"] <- paste(par$ages[i])
    xmlAttrs(env[[i]])["male"] <- paste(par$weights[i, 1])
    xmlAttrs(env[[i]])["female"] <- paste(par$weights[i, 2])
  }
  
  return(xml_doc)
  
}


# Set coverage of one block of PC rounds ----
set_PC_coverage <- function(xml_doc = NULL, par = NULL) {
  
  check_par_names(x = par,
                  required = c("coverage", "fraction_excluded",
                               "systematicness"),
                  fun_name = "set_PC_coverage")
  
  xpath_expr <-  "/wormsim.input/control/mass.treatment/round.table/round"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["coverage"] <- paste(par$coverage)
  xmlAttrs(env[[1]])["fraction.excluded"] <- paste(par$fraction_excluded)
  xmlAttrs(env[[1]])["systematicness"] <- paste(par$systematicness)
  return(xml_doc)
  
}


# Set up diagnostic tests used in surveys ----
set_diagnostics <- function(xml_doc = NULL,
                                par = NULL) {
  
  sapply(par, check_par_names,
         required = c("name", "type", "samples", "distr", "param",
                      "units_of_bio_material"),
         fun_name = "set_diagnostics")
  
  # Check how many placeholders for different diagnostics there are
  xpath_expr <-  "/wormsim.input/diagnostics.table"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  children <- xmlChildren(xml_doc)$wormsim.input[["diagnostics.table"]]
  max_n_diagnostics <- length(xmlChildren(children))
  
  # Trim input (and throw warning) if the number of placeholders cannot
  # accommodate all inputs
  if (length(par) > max_n_diagnostics) {
    par <- par[1:max_n_diagnostics]
    warning(paste0("'set_diagnostics()' will only use the first ",
                   max_n_diagnostics, " specified diagnostics as the xml template does not provide a sufficient number of placeholders"))
  }
  
  # Trim placeholders that will not be filled with inputs (if any)
  if (length(par) < max_n_diagnostics) {
    removeChildren(children,
                   kids = as.list((length(par) + 1):max_n_diagnostics))
  }
  
  # Insert inputs into placeholders
  for (i in 1:length(par)) {
    xpath_expr <-  "/wormsim.input/diagnostics.table/diagnostics"
    env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
    
    xmlAttrs(env[[i]])["name"] <- paste(par[[i]]$name)
    xmlAttrs(env[[i]])["type"] <- paste(par[[i]]$type)
    
    xpath_expr <-  "/wormsim.input/diagnostics.table/diagnostics/test.result"
    env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
    
    xmlAttrs(env[[i]])["samples"] <- paste(par[[i]]$samples)
    xmlAttrs(env[[i]])["dist"] <- paste(par[[i]]$distr)
    xmlAttrs(env[[i]])["param"] <- paste(par[[i]]$param)
    xmlAttrs(env[[i]])["units.of.bio.material"] <- paste(par[[i]]$units_of_bio_material)
  }
  
  return(xml_doc)
  
}


# Set transmission-related interventions ----
set_transm_interv <- function(xml_doc = NULL,
                              par = NULL) {
  
  xpath_expr <-  "/wormsim.input/control/intervention.table/intervention"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  
  if (length(par) != length(env))
    stop("'set_transm_interv': length of argument 'par' must be equal to the number of transmission intervention slots in the xml input template")
  
  for (i in 1:length(par)) {
    check_par_names(x = par[[i]],
                    required = c("start", "type", "efficiency", "fraction_excluded"),
                    fun_name = paste0("set_transm_interv[element", i, "]"))
  
    for (j in 1:length(par[[i]])) {
      xmlAttrs(env[[i]])["start"] <- paste(par[[i]]$start)
      xmlAttrs(env[[i]])["type"] <- paste(par[[i]]$type)
      xmlAttrs(env[[i]])["efficiency"] <- paste(par[[i]]$efficiency)
      xmlAttrs(env[[i]])["fraction.excluded"] <- paste(par[[i]]$fraction_excluded)
    }
  }
  
  return(xml_doc)
  
}


# Set survey sequence for output ----
set_survey_seq <- function(xml_doc = NULL, par = NULL) {
  
  check_par_names(x = par,
                  required = c("start", "stop", "interval"),
                  fun_name = "set_survey_seq")
  
  xpath_expr <-  "/wormsim.output/periodic"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["start"] <- paste(par$start)
  xmlAttrs(env[[1]])["stop"] <- paste(par$stop)
  xmlAttrs(env[[1]])["interval"] <- paste0(par$interval, "m")
  return(xml_doc)
  
}


# Set age categories for output generation ----
set_output_agegroups <- function(xml_doc = NULL,
                                 par = NULL) {
  
  if (any(diff(par) <= 0))
    stop("set_output_agegroups: input must be a vector of strictly increasing values")
  
  # Check how many placeholders there are
  xpath_expr <-  "/wormsim.output/age.table"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  children <- xmlChildren(xml_doc)$wormsim.output[["age.table"]]
  max_rows <- length(xmlChildren(children))
  
  # Trim input (and throw warning) if the number of placeholders cannot
  # accommodate all inputs
  if (length(par) > max_rows) {
    par <- par[1:max_rows]
    warning(paste0("'set_output_agegroups()' will only use the first ",
                   max_rows, " input values provides as the xml template does not provide a sufficient number of placeholders"))
  }
  
  # Trim placeholders that will not be filled with inputs (if any)
  if (length(par) < max_rows) {
    removeChildren(children,
                   kids = as.list((length(par) + 1):max_rows))
  }
  
  # Insert inputs into placeholders
  xpath_expr <-  "/wormsim.output/age.table/age"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  for (i in 1:length(par)) {
    xmlAttrs(env[[i]])["upper.age.limit"] <- paste(par[i])
  }
  
  return(xml_doc)
  
}


# Set up individual output ----
set_output_individual <- function(xml_doc = NULL,
                                  par = NULL) {
  
  check_par_names(x = par,
                  required = c("age_lo", "age_hi", "worm_types"),
                  fun_name = "set_output_individual")
  
  xpath_expr <-  "/wormsim.output/elements/individual"
  env <- get_xml_env(xml_doc = xml_doc, xml_path = xpath_expr)
  xmlAttrs(env[[1]])["lowerAgeLimit"] <- paste(par$age_lo)
  xmlAttrs(env[[1]])["upperAgeLimit"] <- paste(par$age_hi)
  xmlAttrs(env[[1]])["wormTypes"] <- paste(par$worm_types)
  return(xml_doc)
  
}


### END OF CODE ----
