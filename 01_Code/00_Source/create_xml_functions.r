# Load required libraries
  library(XML)

# Create an XML input file, based on the following arguments
#   template = XML template file.
#   xml_name = file name for generated XML input file.
#   pars = named list of named lists with alternative parameter values; only
#          non-NULL elements will be used to redefine parameters in the input 
#          file; assigment of values is based on the element names, using the
#          function "set_[name]".
  gen_xml_file <- function(template,
                           xml_name,
                           pars = NULL) {
    
  # Parse XML template
    xml_doc <- read_xml_file(template)
    
  # Edit the parsed XML file
    for (i in 1:length(pars)) {
      if(!is.null(pars[[i]])) {
        xml_doc <- do.call(
          what = paste("set_", names(pars)[i], sep = ""),
          args = list(xml_doc = xml_doc, par = pars[[i]]))
      }
    }
  
  # Write XML file
    write_xml_file(xml_doc, xml_name)
      
  }
  

# Read an XML input file  
  read_xml_file <- function(filename) {
    
    xmlInternalTreeParse(filename)
    
  }  
  

# Write an XML input file
  write_xml_file <-  function(xml_doc, filename) {
    
    sink(file = filename)
    print(xml_doc)
    sink()
    
  }
  