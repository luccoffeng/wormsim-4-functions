# Default parameters for hookworm transmission (inputs) ----
# Overview of structure of xml file (what is the xml content?): https://gitlab.com/erasmusmc-public-health/wormsim-public/-/wikis/usageguide/input
# Overview of technical specification (what is done with the xml content?): https://gitlab.com/erasmusmc-public-health/wormsim-public/-/wikis/specifications/specifications
# Overview of allowed functions: https://gitlab.com/erasmusmc-public-health/sim-commons.public/-/wikis/specifications/core/input#322-functions

par_def <- list(
  simulation_time = list(start = "1800-01-01",
                         stop = "2002-01-05",
                         update_interval = 25),
  population_max = list(max_size = 500,
                        fraction_removed = 0.1),
  adult_lifespan = list(dist = "weibull",
                        mean = 3,
                        param = 2),
  prepatent_period = 7/52,
  mating = list(insemination_exp = "1",
                male_potention = 100),
  fertility = list(fecundity_lambda = 0,
                   offspring_per_Fworm = 1,
                   dens_dep = list(func = "alternativehyperbolic",  # Defined in terms of epg
                                   a = 200,
                                   b = list(dist = "gamma",
                                            mean = 1500,
                                            param = 50),
                                   c = 1)),
  offspring_prod = list(male_fract = 0.5,
                        age_bin = c(10, 20),
                        production = c(1, 0)),
  offspring_lifespan_max = "1",
  
  transm_rate = 0.001,
  transm_season_multiplier = rep(1, 12),
  expo_het = list(dist = "gamma", mean = 1, param = 0.35, max = 20),
  exposure_age = list(func = "lin", min = 0, max = 1, param = c(0.1, 0)),
  contrib_age = list(func = "lin", min = 0, max = 1, param = c(0.1, 0)),
  succes_prob_in_cloud = 1,
  succes_prob_in_host = 1,
  reservoir_uptake = list(func = "lin", param = c(1, 0)),
  reservoir_surv = list(life_span = 14,  # must be specified in days
                        multiplier = rep(1, 12)),
  foi_ext = list(start = c("1800-01-01", "1805-01-01"),
                 value = c(5, 0),
                 exclusive = c("true", "false")),
  
  treatm_adult_worm_kill = 0.95,
  pc_seq = list(start = "2015-01-01",
                periodicity = 1,  # must be specified in (fraction) of years
                stop = "2019-12-30",
                coverage = 0.2441,
                fraction_excluded = 0,
                systematicness = 0),
  pc_part_weights = list(ages = c(2, 5, 15, 90), 
                         weights = cbind(c(0, 0, 1, 0),  
                                         c(0, 0, 1, 0))),
  
  transm_interv = list(list(start = "2015-01-01",
                            type = "contribution",
                            efficiency = 0,
                            fraction_excluded = 0),
                       list(start =  "2015-01-01",
                            type = "exposure",
                            efficiency = 0,
                            fraction_excluded = 0)))


### END OF CODE ### ----
