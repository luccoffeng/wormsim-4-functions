# Function to run a set of simulations in a subfolder and return results ----
run_wormsim <- function(wormsim_dir,
                        temp_dir = "TEST",
                        input_file,
                        output_file,
                        seed_start,
                        seed_end,
                        par_input = NULL,
                        par_output = NULL,
                        silent = TRUE,
                        write_to_log = FALSE) {
  
  # wormsim_dir = folder containing WORMSIM
  # temp_dir = the temporary subfolder name in which WORMSIM will be run
  # input_file = path of input xml file (character)
  # default_output_file = path of output xml file (character)
  # seed_start = first seed (integer).
  # seed_end = last seed (integer).
  # par_input = optional list of parameter values that will be plugged into the input_file
  # par_output = optional list of parameter values that will be plugged into the output_file
  # silent = logical flag to indicate whether terminal output should be printed to the R console
  # write_to_log = logical flag to indicate whether terminal output be printed to a log file
  #                (if TRUE, the argument 'silent' will be ignored and nothing will be printed
  #                to the R consoler; always set 'write_to_log' to FALSE when
  #                running multiple simulations in parallel as all the parallel
  #                simulations may try to write into the same log file! I have
  #                requested Rinke to change the location of the log file to the
  #                same folder as the .jar file, so that it can be saved by R.).
  ### ACTION: update code after Rinke's fix
  
  # Set up a temporary subfolder
  setwd(wormsim_dir)
  subfolder <- file.path(wormsim_dir, temp_dir)
  dir.create(subfolder, showWarnings = FALSE)
  file.copy(dir(), subfolder, overwrite = TRUE)
  setwd(subfolder)
  dir.create("output", showWarnings = FALSE)  # WORMSIM will write results here
  
  # Copy input and output xml files to temporary subfolder
  file.copy(input_file, subfolder, overwrite = TRUE)
  file.copy(output_file, subfolder, overwrite = TRUE)
  input_file_name <- gsub(".*/", "", input_file)    # i.e. file name without full path
  output_file_name <- gsub(".*/", "", output_file)  # i.e. file name without full path
  
  # Plug in parameter values (optional) and overwrite xml files
  if (!is.null(par_input)) {
    setwd(subfolder)
    gen_xml_file(template = input_file_name,
                 xml_name = input_file_name,
                 pars = par_input)
  }
  if (!is.null(par_output)) {
    setwd(subfolder)
    gen_xml_file(template = output_file_name,
                 xml_name = output_file_name,
                 pars = par_output)
  }
  
  # Run WORMSIM
  setwd(subfolder)
  run(input_file = input_file_name,
      output_file = output_file_name,
      seed_start = seed_start,
      seed_end = seed_end,
      silent = silent,
      write_to_log = write_to_log)
  
  # Read output from disk
  setwd(file.path(subfolder, "output"))
  files_basic <- dir(pattern = "BASIC")
  files_histogram <- dir(pattern = "HISTOGRAM")
  files_individual <- dir(pattern = "INDIVIDUAL")
  
  seeds <- sub(sub(".xml", "", input_file_name),
               "",
               files_basic)
  seeds <- as.integer(sub("BASIC.csv", "", seeds))
  
  output_basic <- lapply(files_basic, fread)
  output_histogram <- lapply(files_histogram, fread)
  
  for (i in 1:length(seeds)) {
    output_basic[[i]][, seed := seeds[i]]
    output_histogram[[i]][, seed := seeds[i]]
  }
  
  if (length(files_individual) > 0) {
    output_individual <- lapply(files_individual, fread)
    for (i in 1:length(seeds)) {
      output_individual[[i]][, seed := seeds[i]]
    }
  }
  
  
  # Clean up
  setwd(wormsim_dir)
  unlink(subfolder, recursive = TRUE)
  
  # Return result
  temp <- list(basic = rbindlist(output_basic),
               histo = rbindlist(output_histogram))
  
  if (length(files_individual) > 0) {
    temp$individual <- rbindlist(output_individual)
  }
  
  return(temp)
  
}


# Function to run simulations in WORMSIM ----
run <- function(input_file,
                output_file,
                seed_start,
                seed_end,
                silent = TRUE,  # toggle to print information to R console
                write_to_log = FALSE) {  # toggle to print information to log file
  
  if (.Platform$OS.type == "unix") {
    run_command <- if (write_to_log) {
      "./runFromR.sh"  # Directs console output to log file on disk
    } else {
      "./run.sh"       # Prints console output to R
    }
  }
  if (.Platform$OS.type == "windows") {
    run_command <- if (write_to_log) {
      "runFromR.bat"  # Directs console output to log file on disk
    } else {
      "run.bat"       # Prints console output to R
    }
  }
  
  system(command = paste(run_command,
                         input_file,
                         output_file,
                         seed_start,
                         seed_end),
         wait = TRUE,
         ignore.stdout = silent,
         ignore.stderr = silent)
  
}


# Function to simulate the expected number of eggs per gram faeces (epg) ----
gen_epg_expect <- function(wf_insem, a, b, c, shape_b) {
  
  # wf_insem = number of inseminated female worms
  # a, b, shape_b = parameters for the density-dependent association between
  # number of inseminated female worms (hyperbolic saturation) with epg/fworm of
  # a, saturation level b, and individual variation in saturation level following 
  # gamma distribution with with shape parameter shape_b, and a multiplier c
  # that scales the overall expected epg based on egg recovery rate of a 
  # particular diagnostic test
  
  n <- length(wf_insem)
  if (shape_b < Inf) {
    b <- b * rgamma(n, shape = shape_b, rate = shape_b)
  }
  c * a * wf_insem / (1 + a * wf_insem / b)
}


# Function to generate epg test results from individual level output ----
gen_offspring_survey <- function(n_wf,
                                 n_wm,
                                 name_diagn,
                                 a = NULL,
                                 b = NULL,
                                 c = NULL,
                                 shape_b = NULL,
                                 lambda = NULL,
                                 z = NULL,
                                 weight_sample,
                                 n_day,
                                 n_test,
                                 k_day,
                                 k_test,
                                 return_mean = TRUE) {
  
  # n_wf = integer vector of number of female worms per host,
  # n_wm = integer vector of number of male worms per host,
  # name_diagn = character vector with names of different diagnostics to
  # simulate; if the length of this vector is >1, all of the remaining arguments
  # related to diagnostics (lambda, c, weight_sample, n_day, n_test, k_day, and
  # k_test) may optionally be specified as a vector of equal length as
  # names_diagn (or must be length 1 and will be recycled across diagnostics).
  
  # a, b, c, shape_b = parameters for the density-dependent association between
  # number of inseminated female worms (hyperbolic saturation) with epg/fworm of
  # a, saturation level b, and individual variation in saturation level following 
  # gamma distribution with with shape parameter shape_b, and a multiplier c
  # that scales the overall expected epg based on egg recovery rate of a 
  # particular diagnostic test. The unit of parameters a and b must be "X (e.g., 
  # eggs) per weight of sample tested", where the unit of "weight" is the same
  # as the unit of the function argument 'weight_sample' (below). Optionally,
  # set shape_b to Inf to turn off inter-individual heterogeneity in the
  # saturation level of egg production (parameter 'b').
  
  # lambda and z are an alternative parameterisation of density-dependent
  # fecundity as implemented in some models for STH and SCH. Parameter lambda
  # represents the expected number of X (e.g., eggs) per weight of sample
  # tested, and parameter z governs the degree of density dependence in an
  # exponential fashion: expected X = lambda * W_i * exp[-z * W_i], where W_i is
  # the number of mated female worms. As soon as lambda and z or specified
  # (i.e., they are not 'NULL'), parameters a, b, c, and b_shape are ignored.
  
  # weight_sample = weight of sample used per test
  # n_day = the maximum number of days that a person is tested
  # n_test = the maximum number of repeated tests per day
  # k_day = the overdispersion of test results on different days (shape of gamma distribution)
  # k_test = the overdispersion of test results on the same day (shape of gamma distribution), conditional on the specified weight of sample
  # If a k parameter is Inf, no variation is assumed (due to that aspect). If
  # both k parameters are Inf, the function will return a poisson draw
  
  # General prep and checks of input arguments
  if (!all(round(n_wf) == n_wf) || !all(round(n_wm) == n_wm))
    stop("'n_wf' and 'n_wm' must be integer values")
  
  n_diagn <- length(name_diagn)
  
  if (is.null(lambda) && is.null(z)) {
    
    # Checks for hyperbolic saturating density-dependent fecundity
    if (!all(sapply(list(c, weight_sample, k_test), length) %in% c(1L, n_diagn)))
      stop(paste0("the following diagnostic arguments must have length 1 or ",
                  "length equal to the length of 'name_diagn': c, ",
                  "weight_sample, and k_test)"))
    
    if (!all(sapply(list(a, b, shape_b, k_day, n_day, n_test), length) == 1))
      stop(paste0("the following arguments must have length 1: a, b, shape_b, ",
                  "k_day (i.e., these cannot vary between diagnostics), and ",
                  "n_day and n_test (which indicate maximum values)"))
    
    if (n_diagn > 1) {
      # Ensure that all diagnostic-related arguments (except those that must be
      # of length 1) are vectors of equal length as n_diagn (with recycling)
      for (variable in c("c", "weight_sample", "k_test")) {
        if(length(get(variable)) != n_diagn)
          assign(x = variable, value = rep(get(variable), n_diagn))
      }
    }
    
    if (any(sapply(list(a, b, c, shape_b, weight_sample,
                        n_day, n_test, k_day, k_test),
                   function(x) any(x <= 0))))
      stop(paste0("all diagnostic arguments (a, b, c, shape_b, weight_sample, ",
                  "n_day, n_test, k_day, and k_test) must be >0"))
    
  } else {
    
    # Checks for exponential saturating density-dependent fecundity
    if (!(!is.null(lambda) & !is.null(z)))
      stop("When speficying lambda or z, both must be specified")
    
    warning("parameters a, b, c, and shape_b will be ignored (you specified lambda and/or z)")
    
    if (!all(sapply(list(lambda, weight_sample, k_test), length) %in% c(1L, n_diagn)))
      stop(paste0("the following diagnostic arguments must have length 1 or ",
                  "length equal to the length of 'name_diagn': lambda, ",
                  "weight_sample, n_day, n_test, and k_test)"))
    
    if (!all(sapply(list(z, k_day, n_day, n_test), length) == 1))
      stop(paste0("the following arguments must have length 1: z and k_day",
                  " (i.e., these cannot vary between diagnostics), and n_day ",
                  "and n_test (which indicate maximum values)"))
    
    if (n_diagn > 1) {
      # Ensure that all diagnostic-related arguments (except those that must be of
      # length 1) are vectors of equal length as n_diagn (with recycling)
      for (variable in c("lambda", "weight_sample", "k_test")) {
        if(length(get(variable)) != n_diagn)
          assign(x = variable, value = rep(get(variable), n_diagn))
      }
    }
    
    if (any(sapply(list(lambda, z, weight_sample, n_day, n_test, k_day, k_test),
                   function(x) any(x <= 0))))
      stop(paste0("all diagnostic arguments (lambda, z, weight_sample, ",
                  "n_day, n_test, k_day, and k_test) must be >0"))
    
  }
  
  if (!all(round(n_day) == n_day) || !all(round(n_test) == n_test))
    stop("'n_day' and 'n_test' must be integer values")
  
  # Calculate number of inseminated female worms per host, assuming that
  # presence of at least one male worm suffices to inseminate all female worms
  wf_insem <- n_wf
  wf_insem[n_wm == 0] <- 0
  
  # Calculate number of eggs per test per individual
  n_ind <- length(wf_insem)
  max_day <- max(n_day)
  max_test <- max(n_test)
  
  test_result <- lapply(1:n_diagn, function(i) {
    
    # Calculate expected number of eggs per diagnostic
    if (!is.null(lambda) & !is.null(z)) {
      egg_count_expect <- lambda[i] * wf_insem * exp(-z * wf_insem) *
        weight_sample[i]
    } else {
      egg_count_expect <- gen_epg_expect(wf_insem = wf_insem,
                                         a = a,
                                         b = b,
                                         c = c[i],
                                         shape_b = shape_b) *
        weight_sample[i]
    }
    
    # Simulate expected number of eggs per day
    egg_count_expect <- 
      if (k_day < Inf) {
        egg_count_expect * matrix(rgamma(n = n_ind * max_day,
                                         shape = k_day,
                                         rate = k_day),
                                  ncol = max_day)
      } else {
        matrix(rep(egg_count_expect, times = max_day),
               ncol = max_day)
      }
    
    # Simulate expected number of eggs per day and per repeated test
    egg_count_expect <-   
      if (k_test[i] < Inf) {
        c(egg_count_expect) * matrix(rgamma(n = n_ind * max_day * max_test,
                                            shape = k_test[i],
                                            rate = k_test[i]),
                                     ncol = max_day * max_test) 
      } else {
        matrix(rep(egg_count_expect, times = max_test),
               ncol = max_day * max_test)
      }
    
    # Simulate egg count per day and repeated test, and convert to offspring
    # density per unit sample weight
    epg <-
      matrix(rpois(n = n_ind * max_day * max_test,
                   lambda = egg_count_expect),
             ncol = max_day * max_test,
             dimnames = list(NULL, paste0("epg_",
                                          name_diagn[i],
                                          "_day",
                                          rep(1:max_day, times = max_test),
                                          "_test",
                                          rep(1:max_test, each = max_day)))) /
      weight_sample[i]
    
  })
  
  names(test_result) <- name_diagn
  
  # Calculate mean, if needed
  if (return_mean) {
    days <- rep(1:max_day, times = max_test)
    tests <- rep(1:max_test, each = max_day)
    
    test_result <- lapply(test_result, function(x) {
      
      sapply(1:length(days), function(i) {
        
        cols_to_get <- paste0("day", rep(1:days[i], times = tests[i]),
                              "_test", rep(1:tests[i], each = days[i]))
        cols_to_get <- 
          grep(paste(cols_to_get, collapse = "|"), colnames(x), value = TRUE)
        
        apply(x[, cols_to_get, drop = FALSE], 1, mean)
        
      })
      
    })
    
    # Label columns in format "AxB" (A = number of days, B = number of tests)
    for (i in 1:length(test_result)) {
      colnames(test_result[[i]]) <- paste0("epg_", names(test_result[i]),
                                           "_", days, "x", tests)
    }
    
    return(do.call(cbind, test_result))
    
  }
  
  # Or return result without taking mean
  do.call(cbind, test_result)
  
}

# # Check that function works
# gen_offspring_survey(n_wf = 0:10,
#                      n_wm = rep(1, 11),
#                      name_diagn = c("kk", "pcr"),
#                      a = 200, b = 1500, c = c(1, 12.3), shape_b = 50,
#                      # lambda = 0.37, z = 7e-4,
#                      weight_sample = c(1/24, 0.2),
#                      n_day = 2,
#                      n_test = 2,
#                      k_day = 1,
#                      k_test = c(0.31, Inf),
#                      return_mean = TRUE)

### END OF CODE ### ----
