# Function to create a list of all combinations of alternative parameter ----
create_combi_list <- function(par_alt) {
  
  # par_alt = named list of vectors with alternative parameter values.
  
  n_alt_par_values <- sapply(par_alt, length)
  n_alt_par_comb <- prod(n_alt_par_values)

# Initialize list of combinations of parameter values and determine indices
# needed to pull values from par_alt.
  index_alt_par <- expand.grid(lapply(par_alt, function(x) {1:length(x)}))
  par_alt_combi_list <- as.list(rep(list(as.list(rep(NA, dim(index_alt_par)[2]))), n_alt_par_comb))

# Fill list with unique combinations of parameter values
  for (i in 1:dim(index_alt_par)[1]) {
    for (j in 1:dim(index_alt_par)[2]) {        
      
      par_alt_combi_list[[i]][[j]] <- par_alt[[j]][[index_alt_par[i,j]]]
        
    }
    
    names(par_alt_combi_list[[i]]) <- names(par_alt)
    
  }
  
# Return list of lists of alternative parameter values
  return(par_alt_combi_list)

}


### END OF CODE ### ----
